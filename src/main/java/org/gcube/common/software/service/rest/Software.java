package org.gcube.common.software.service.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.HEAD;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.software.analyser.Analyser;
import org.gcube.common.software.analyser.AnalyserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path("software")
public class Software {
	
	private final Logger logger = LoggerFactory.getLogger(Software.class);
	
	public static final String APPLICATION_JSON_CHARSET_UTF_8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	
	@HEAD
	public Response alive() {
		return Response.status(Status.OK).build(); 
	}
	
	
	@POST
	@Consumes(APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 202, condition = "The provided json is formally correct.")
	})
	public Response create(String json) {
		try {
			Analyser analyser = AnalyserFactory.getAnalyser(json);
			Thread thread = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
						analyser.analyse();
					} catch (Exception e) {
						logger.error("Error while analysing\n{}", json);
					}
			    }
			});  
			thread.start();
			return Response.status(Status.ACCEPTED).build(); 
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	
}
