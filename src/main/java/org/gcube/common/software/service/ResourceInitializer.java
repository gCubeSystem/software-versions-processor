package org.gcube.common.software.service;

import javax.ws.rs.ApplicationPath;

import org.gcube.common.software.service.rest.Software;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
public class ResourceInitializer extends ResourceConfig {
	
	public ResourceInitializer() {
		packages(Software.class.getPackage().toString());
	}
	
}
