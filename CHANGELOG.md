This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Software Versions Processor

## [v1.0.0-SNAPSHOT]

- First Release

